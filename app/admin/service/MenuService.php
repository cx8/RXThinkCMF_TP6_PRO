<?php
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\service;


use app\admin\model\Menu;
use app\common\service\BaseService;

/**
 * 菜单管理-服务类
 * @author 牧羊人
 * @since: 2020/6/30
 * Class MenuService
 * @package app\admin\service
 */
class MenuService extends BaseService
{
    /**
     * 构造函数
     * MenuService constructor.
     */
    public function __construct()
    {
        $this->model = new Menu();
    }

    /**
     * 获取数据列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @since: 2020/6/30
     * @author 牧羊人
     */
    public function getList()
    {
        $list = $this->model->getList([], 'sort asc');
        if ($list) {
            foreach ($list as &$val) {
                if (intval($val['type']) <= 2) {
                    $val['open'] = true;
                } else {
                    $val['open'] = false;
                }
            }
        }
        return message("操作成功", true, $list);
    }

    /**
     * 添加或编辑
     * @return array
     * @throws \think\db\exception\BindParamException
     * @author 牧羊人
     * @since 2020/7/3
     */
    public function edit()
    {
        // 请求参数
        $data = request()->param();
        $result = $this->model->edit($data);
        if (!$result) {
            return message("操作失败", false);
        }

        // 上级菜单设置隐藏和显示，默认同步更新子级菜单
        $menuList = $this->model->getChilds($result, true);
        foreach ($menuList as $val) {
            // 设置状态值
            $menuMod = new Menu();
            $v = [
                'id' => $val['id'],
                'status' => $data['status'],
                'is_public' => $data['is_public'],
            ];
            $menuMod->edit($v);

            // 获取子级
            $children = $val['children'];
            if (is_array($children) && !empty($children)) {
                foreach ($children as $vt) {
                    $item = [
                        'id' => $vt['id'],
                        'status' => $data['status'],
                        'is_public' => $data['is_public'],
                    ];
                    $menuMod = new Menu();
                    $menuMod->edit($item);

                    // 更新子级菜单
                    $children2 = $vt['children'];
                    foreach ($children2 as $vo) {
                        $subItem = [
                            'id' => $vo['id'],
                            'status' => $data['status'],
                            'is_public' => $data['is_public'],
                        ];
                        $menuMod = new Menu();
                        $menuMod->edit($subItem);
                    }
                }
            }
        }

        // 节点参数
        $func = isset($data['func']) ? $data['func'] : "";
        // URL地址
        $url = trim($data['url']);
        if ($data['type'] == 3 && $func && $url) {
            $item = explode("/", $url);
            if (count($item) == 3) {

                // 删除以存在的节点
                $funcIds = $this->model
                    ->where("pid", $result)
                    ->where("type", 4)
                    ->column("id");
                $this->model->deleteDAll($funcIds, true);

                // 模块名
                $module = $item[1];
                $funcList = explode(",", $func);
                foreach ($funcList as $val) {
                    $data = [];
                    $data['pid'] = $result;
                    $data['type'] = 4;
                    $data['status'] = 1;
                    $data['is_public'] = 2;
                    $data['sort'] = $val;

                    if ($val == 1) {
                        // 列表
                        $data['name'] = "列表";
                        $data['url'] = "/{$module}/list";
                        $data['permission'] = "sys:{$module}:list";
                    } else if ($val == 5) {
                        // 添加
                        $data['name'] = "添加";
                        $data['url'] = "/{$module}/edit";
                        $data['permission'] = "sys:{$module}:add";
                    } else if ($val == 10) {
                        // 修改
                        $data['name'] = "修改";
                        $data['url'] = "/{$module}/edit";
                        $data['permission'] = "sys:{$module}:edit";
                    } else if ($val == 15) {
                        // 删除
                        $data['name'] = "删除";
                        $data['url'] = "/{$module}/drop";
                        $data['permission'] = "sys:{$module}:drop";
                    } else if ($val == 20) {
                        // 详情
                        $data['name'] = "详情";
                        $data['url'] = "/{$module}/detail";
                        $data['permission'] = "sys:{$module}:detail";
                    } else if ($val == 25) {
                        // 状态
                        $data['name'] = "状态";
                        $data['url'] = "/{$module}/setStatus";
                        $data['permission'] = "sys:{$module}:status";
                    } else if ($val == 30) {
                        // 批量删除
                        $data['name'] = "批量删除";
                        $data['url'] = "/{$module}/batchDrop";
                        $data['permission'] = "sys:{$module}:dall";
                    } else if ($val == 35) {
                        // 添加子级
                        $data['name'] = "添加子级";
                        $data['url'] = "/{$module}/addz";
                        $data['permission'] = "sys:{$module}:addz";
                    } else if ($val == 40) {
                        // 全部展开
                        $data['name'] = "全部展开";
                        $data['url'] = "/{$module}/expand";
                        $data['permission'] = "sys:{$module}:expand";
                    } else if ($val == 45) {
                        // 全部折叠
                        $data['name'] = "全部折叠";
                        $data['url'] = "/{$module}/collapse";
                        $data['permission'] = "sys:{$module}:collapse";
                    } else if ($val == 50) {
                        // 导入数据
                        $data['name'] = "导入数据";
                        $data['url'] = "/{$module}/import";
                        $data['permission'] = "sys:{$module}:import";
                    } else if ($val == 55) {
                        // 导出数据
                        $data['name'] = "导出数据";
                        $data['url'] = "/{$module}/export";
                        $data['permission'] = "sys:{$module}:export";
                    } else if ($val == 60) {
                        // 设置权限
                        $data['name'] = "设置权限";
                        $data['url'] = "/{$module}/permission";
                        $data['permission'] = "sys:{$module}:permission";
                    } else if ($val == 65) {
                        // 设置权限
                        $data['name'] = "重置密码";
                        $data['url'] = "/{$module}/resetPwd";
                        $data['permission'] = "sys:{$module}:resetPwd";
                    }
                    $menuMod = new Menu();
                    $menuMod->edit($data);
                }
            }
        }
        return message();
    }

}