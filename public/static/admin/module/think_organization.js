// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 配置管理
 * @author 牧羊人
 * @since 2021/7/26
 */
layui.use(['layer', 'form', 'table', 'util', 'admin', 'tree', 'xmSelect', 'treeTable', 'common'], function () {
    var $ = layui.jquery;
    var layer = layui.layer;
    var form = layui.form;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var tree = layui.tree;
    var xmSelect = layui.xmSelect;
    var selObj, treeData;  // 左树选中数据
    var common = layui.common;

    /* 渲染树形 */
    function renderTree() {
        $.ajax({
            url: "/dept/index",
            dataType: "json",
            type: "POST",
            data: {},
            beforeSend: function () {

            },
            success: function (res) {
                if (res.success) {
                    for (var i = 0; i < res.data.length; i++) {
                        res.data[i].title = res.data[i].name;
                        res.data[i].id = res.data[i].id;
                        res.data[i].spread = true;
                    }
                    treeData = layui.treeTable.pidToChildren(res.data, 'id', 'pid');
                    tree.render({
                        elem: '#organizationTree',
                        onlyIconControl: true,
                        data: treeData,
                        click: function (obj) {
                            selObj = obj;
                            $('#organizationTree').find('.ew-tree-click').removeClass('ew-tree-click');
                            $(obj.elem).children('.layui-tree-entry').addClass('ew-tree-click');
                            insTb.reload({
                                where: {deptId: obj.data.id},
                                page: {curr: 1},
                                method: 'post',
                                url: '/admin/index'
                            });
                        }
                    });
                    $('#organizationTree').find('.layui-tree-entry:first>.layui-tree-main>.layui-tree-txt').trigger('click');
                } else {
                    layer.msg(res.msg, {icon: 5});
                    return false;
                }
            },
            error: function () {
                layer.msg("AJAX请求异常");
            }
        });
    }

    renderTree();

    /* 添加 */
    $('#organizationAddBtn').click(function () {
        showEditModel(null, selObj ? selObj.data.parentId : null);
    });

    /* 修改 */
    $('#organizationEditBtn').click(function () {
        if (!selObj) return layer.msg('未选择机构', {icon: 2});
        showEditModel(selObj.data);
    });

    /* 删除 */
    $('#organizationDelBtn').click(function () {
        if (!selObj) return layer.msg('未选择机构', {icon: 2});
        doDel(selObj);
    });

    /* 显示表单弹窗 */
    function showEditModel(mData, pid) {
        admin.open({
            type: 1,
            area: '600px',
            title: (mData ? '修改' : '添加') + '机构',
            content: $('#organizationEditDialog').html(),
            success: function (layero, dIndex) {
                // 回显表单数据
                form.val('organizationEditForm', mData);
                // 表单提交事件
                form.on('submit(organizationEditSubmit)', function (data) {
                    data.field.pid = insXmSel.getValue('valueStr');
                    var loadIndex = layer.load(2);
                    $.post('/dept/edit', data.field, function (res) {
                        layer.close(loadIndex);
                        if (0 == res.code) {
                            layer.close(dIndex);
                            layer.msg(res.msg, {icon: 1});
                            renderTree();
                        } else {
                            layer.msg(res.msg, {icon: 2});
                        }
                    }, 'json');
                    return false;
                });
                // 渲染下拉树
                var insXmSel = xmSelect.render({
                    el: '#organizationEditParentSel',
                    height: '250px',
                    data: treeData,
                    initValue: mData ? [mData.pid] : (pid ? [pid] : []),
                    model: {label: {type: 'text'}},
                    prop: {
                        name: 'name',
                        value: 'id'
                    },
                    radio: true,
                    clickClose: true,
                    tree: {
                        show: true,
                        indent: 15,
                        strict: false,
                        expandedKeys: true
                    }
                });
                // 禁止弹窗出现滚动条
                $(layero).children('.layui-layer-content').css('overflow', 'visible');
            }
        });
    }

    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要删除此机构吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            $.get('../../json/ok.json', {
                id: obj.data.id,
            }, function (res) {
                layer.close(loadIndex);
                if (0 == res.code) {
                    layer.msg(res.msg, {icon: 1});
                    renderTree();
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });
    }

    /* 渲染表格 */
    var insTb = table.render({
        elem: '#organizationUserTable',
        data: [],
        height: 'full-52',
        page: true,
        toolbar: '#organizationUserTbToolBar',
        cellMinWidth: 100,
        cols: [[
            {type: 'checkbox'},
            , {field: 'username', width: 200, title: '用户名', align: 'center'}
            , {field: 'realname', width: 200, title: '真实姓名', align: 'center'}
            , {field: 'gender', width: 100, title: '性别', align: 'center', templet: function (d) {
                    var str = "";
                    if (d.gender == 1) {
                        str = '<span class="layui-btn layui-btn-normal layui-btn-xs">男</span>';
                    } else {
                        str = '<span class="layui-btn layui-btn-normal layui-btn-xs layui-btn-danger">女</span>';
                    }
                    return str;
                }
            }
            , {field: 'mobile', width: 200, title: '手机号码', align: 'center'}
            , {field: 'email', width: 200, title: '邮箱', align: 'center',}
            , {title: '操作', toolbar: '#organizationUserTbBar', align: 'center', width: 120, minWidth: 120}
        ]],
        done: function () {
            // 表格搜索
            form.on('submit(organizationUserTbSearch)', function (data) {
                insTb.reload({where: data.field, page: {curr: 1}});
                return false;
            });
        }
    });

    /* 表格工具条点击事件 */
    table.on('tool(organizationUserTable)', function (obj) {
        if (obj.event === 'edit') { // 修改
            showEditModel2(obj.data);
        } else if (obj.event === 'del') { // 删除
            doDel2(obj);
        }
    });

    /* 表格头工具栏点击事件 */
    table.on('toolbar(organizationUserTable)', function (obj) {
        if (obj.event === 'add') { // 添加
            showEditModel2();
        } else if (obj.event === 'del') { // 删除
            var checkRows = table.checkStatus('organizationUserTable');
            if (checkRows.data.length === 0) {
                layer.msg('请选择要删除的数据', {icon: 2});
                return;
            }
            var ids = checkRows.data.map(function (d) {
                return d.id;
            });
            doDel2({ids: ids});
        }
    });

    /* 显示表单弹窗2 */
    function showEditModel2(mData) {
        admin.open({
            type: 1,
            title: (mData ? '修改' : '添加') + '用户',
            content: $('#organizationUserEditDialog').html(),
            success: function (layero, dIndex) {
                // 回显表单数据
                form.val('organizationUserEditForm', mData);
                // 表单提交事件
                form.on('submit(organizationUserEditSubmit)', function (data) {
                    var loadIndex = layer.load(2);
                    $.post('/admin/editUser', data.field, function (res) {
                        layer.close(loadIndex);
                        if (0 == res.code) {
                            layer.close(dIndex);
                            layer.msg(res.msg, {icon: 1});
                            insTb.reload({page: {curr: 1}});
                        } else {
                            layer.msg(res.msg, {icon: 2});
                        }
                    }, 'json');
                    return false;
                });
                // 禁止弹窗出现滚动条
                $(layero).children('.layui-layer-content').css('overflow', 'visible');
            }
        });
    }

    /* 删除2 */
    function doDel2(obj) {
        layer.confirm('确定要删除选中用户吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            var ids = []
            if (obj.data) {
                ids = [obj.data.id]
            } else if (obj.ids) {
                ids = obj.ids;
            }
            $.post('/admin/batchDrop', {id: ids.join(",")}, function (res) {
                layer.close(loadIndex);
                if (0 == res.code) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });
    }

});
